//Hai Hoang Do 2237535
package application;
import vehicles.Bicycle;

public class BikeStore
{
        public static void main(String[] args)
    {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Trek", 10, 10);
        bikes[1] = new Bicycle("Canyon", 20, 20);
        bikes[2] = new Bicycle("Cannondale", 30, 30);
        bikes[3] = new Bicycle("Bianchi", 40, 40);

        for(int i = 0; i < bikes.length; i++)
        {
            System.out.println(bikes[i]);
        }
    }
}
