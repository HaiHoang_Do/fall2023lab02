//Hai Hoang Do 2237535
package vehicles;

public class Bicycle
{
    private String manufacturer;
    private int  numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed)
    {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString()
    {
        String returnString = ("Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", Max Speed: " + this.maxSpeed);
        return returnString;
    }

    public String getManufacturer()
    {
        return this.manufacturer;
    }

    public int getNumberGears()
    {
        return this.numberGears;
    }

    public double getMaxSpeed()
    {
        return this.maxSpeed;
    }


}